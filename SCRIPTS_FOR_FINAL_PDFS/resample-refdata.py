#! /usr/bin/env python

import yoda
#print(yoda.__version__)

import argparse
ap = argparse.ArgumentParser()
ap.add_argument("REFDIR")
ap.add_argument("NRESAMPLE", type=int)
args = ap.parse_args()

import random, math, os
for i in range(args.NRESAMPLE):
    #outdir = "ref{:02d}".format(i)
    outdir = args.REFDIR + "_{:02d}".format(i)
    os.mkdir(outdir)

    for yfile in os.listdir(args.REFDIR):
        if not yfile.endswith(".yoda"):
            continue
        aos = yoda.read(os.path.join(args.REFDIR, yfile))

        for path, ao in aos.items():
            #print(ao.path(), ao.type())
            if ao.hasAnnotation("ErrorBreakdown"):
                ao.rmAnnotation("ErrorBreakdown")
            if "Scatter" in ao.type():
                d = ao.dim()
                for j, p in enumerate(ao.points()):
                    #print(d, p.val(d), p.errAvg(d))
                    #print(ao.path(), j, p)
                    p.setVal(d, random.gauss(p.val(d), p.errAvg(d)))
                    p.setErr(d, p.errAvg(d)*math.sqrt(2))
                    ##p.setVal(d, 1.234)
                    #print(ao.path(), j, p)
                    #print()

        yoda.write(aos, os.path.join(outdir, yfile))
    #del aos
