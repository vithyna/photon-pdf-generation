# photon-pdf-generation

To replace the older and badly maintained apfel-code-for-photon-pdfs. Also replaces sorting-rivet-runs git repo.
This repository will contain code for pdf generation, script for automated generation of hundreds of professor sampled PDFs, and the generation of final PDFs and replica sets.

## Procedure
Use prof\_pdfs directory, and use the script to prof2-sample points, generate PDFs and cp to lhapdf directory. It will also copy the parameters to final\_pdfs directory.

then using final\_pdfs directory, do the sorting of all the yoda-files using the ```splitnsort``` script, do all ipol and other testing with weightfiles etc, until you have what you need. Rename this weightfile as weights.dat, and run the resample_refdata script to generate replica datasets, use prof-tune all (can submit to grid) to tune all the replicas as well (you have to tune the nominal yourself) and finaly, use the generate final pdfs script to generate the final pdfs!

## Notes on performance
Default ```APFEL``` subgrid looks like the following:
```
  APFEL::SetNumberOfGrids(3);
  APFEL::SetGridParameters(1, 80, 3, 1e-5);
  APFEL::SetGridParameters(2, 50, 5, 1e-1);
  APFEL::SetGridParameters(3, 40, 5, 8e-1);
```
There seens to be some limit of 200 combined points here, but seems loose? Needs a bit of trial and error to get a combination of subgrids that actually runs. Don't know if this can be fixed by using more grids - if we do need more points.

 - using standard 300(xs) x 150 (qs) grid with default subgrids in apfel ~ 4mins per PDF (7.2MB)
 - using standard 300(xs) x 150 (qs) grid with 80,100,40 subgrids in apfel ~ 5mins 30 per PDF
 - using 150 (xs) x 50 (qs) grid with the above altered subgrids ~ 40s per PDF 


## Notes on settings 
setPertubativeOrder 0 is LO, 1 is NLO, 2 is N2LO

