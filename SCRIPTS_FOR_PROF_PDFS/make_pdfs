#!/bin/bash

# Script does:
#   Takes command line PPE or HOME to identify correct paths as first Arg
#   Runs Apfel with PDF parameters supplied via command line, in order of PDF_APL PDF_BPL PDF_AHAD PDF_BHAD PDF_CHAD PDF_AHADG PDF_BHADG PDF_CHADG
#   OR
#   Runs Apfel by first running professor from command line to recieve the above arguments
#   Copies PDF(s) and info file into LHAPDF PDF library as TestPDF after clearing out TestPDF that already exists
#   Runs an LHAPDF_ graphing script (optionally)
#   Runs Sherpa using newly produced PDF for various Rivet analyses
#   Combines and makes Analyses
#   In the future:
#   Uses analyses to parametrize newer values with professor
#   Repeat a certain number of times


RUN_NUM=7

# For Multithreading, specify N, number of threads

N=155

# Number of members to generate
num_members=400

param_names=("PDF_APL" "PDF_BPL" "PDF_AHAD" "PDF_BHAD" "PDF_AHADG" "PDF_BHADG")

###################################################################################################################################################
## Input Handling
if [[ $1 == "PPE" ]]; then
  LHAPATH="/home/ppe/a/anarendran/Documents/CEDAR_stuff/LHAPDF-6.5.3/local/"
  APFELPATH="/home/ppe/a/anarendran/Documents/CEDAR_stuff/apfel/local/"
  CEDARPATH="/home/ppe/a/anarendran/Documents/CEDAR_stuff/"
elif [[ $1 == "HOME" ]]; then
  LHAPATH="/home/anarendran/Documents/CEDAR_stuff/LHAPDF-6.5.3/install/"
  APFELPATH="/home/anarendran/Documents/CEDAR_stuff/apfel/local/"
  CEDARPATH="/home/anarendran/Documents/CEDAR_stuff/"
elif [[ $1 == "Ox" ]]; then
  export LHAPATH="/home/narendran/Documents/CEDAR_stuff2/lhapdf/"
  export APFELPATH="/home/narendran/Documents/CEDAR_stuff2/apfel_install/apfel/local/"
  export CEDARPATH="/home/narendran/Documents/CEDAR_stuff2/"
  wd=/home/narendran/Documents/CEDAR_stuff2/photon-pdf-generation/SCRIPTS_FOR_PROF_PDFS/ALL_RUNS/RUN$RUN_NUM
else
  echo "Specify Environment as either PPE or HOME or Ox in command line like : ./make_PDF PPE"
  exit 1
fi

mkdir -p $wd
pdf_name=PhotonPDF_VariedR$RUN_NUM

#Generate PDF params using professor
prof2-sample -n $num_members -o $wd/prof-parametersR$RUN_NUM /home/narendran/Documents/CEDAR_stuff2/photon-pdf-generation/SCRIPTS_FOR_PROF_PDFS/professor/parameter.txt

mv $wd/prof-parametersR$RUN_NUM/points.dat $wd/

num_members=$(ls $wd/prof-parametersR$RUN_NUM | wc -l)

num_mem_for_iter=$(($num_members - 1))

# Copy the newest version of the PDF generation code
cp /home/narendran/Documents/CEDAR_stuff2/photon-pdf-generation/APFEL_CODE/apfel_* $wd/
## Compiling Apfel
g++ $wd/apfel_lhaout.cc -o $wd/apfel-lhaout-cc -I"$APFELPATH"include -L"$APFELPATH"lib64 -L"$LHAPATH"lib -lAPFEL -lLHAPDF
g++ $wd/apfel_infoout.cc -o $wd/apfel-infoout-cc -I"$APFELPATH"include -L"$APFELPATH"lib64 -L"$LHAPATH"lib -lAPFEL -lLHAPDF

rm -rf $wd/$pdf_name*
mkdir $wd/$pdf_name
# Clean Test_PDFs left in the parameters directory in the event that the script was stopped prematurely
rm -rf $wd/prof-parametersR$RUN_NUM/"$pdf_name"_*
## Pulling parameters from professor
#cd prof-parametersR5
cd $wd
# Associative arrays!
declare -A params

for i in $(seq -f "%04g" 0 $num_mem_for_iter); do
  ((j=j%N)); ((j++==0)) && wait
  file=$wd/prof-parametersR$RUN_NUM/$i/params.dat
  for param in "${param_names[@]}"; do
    value=$(grep -w "^$param" $file | awk '{print $2}')
    params[$param]=$value
  done
  mem_number=$i
  ## Running Apfel
  PDF_APL=${params[PDF_APL]} PDF_BPL=${params[PDF_BPL]} PDF_AHAD=${params[PDF_AHAD]} PDF_BHAD=${params[PDF_BHAD]} PDF_AHADG=${params[PDF_AHADG]} PDF_BHADG=${params[PDF_BHADG]} mem_number=$mem_number pdf_name=$pdf_name $wd/apfel-lhaout-cc &
  echo "Running PDF $i/$num_members"
done

wait

mv $wd/*.dat $wd/$pdf_name/
## Run Apfel for info file
cd $wd
num_members=$num_members pdf_name=$pdf_name $wd/apfel-infoout-cc
wait

mv $wd/*.info $wd/$pdf_name/
## Clean LHAPDF PDF library of TestPDF
rm -rf "$LHAPATH"share/LHAPDF/$pdf_name

## Add newly generated PDFs to LHAPDF
mv $pdf_name "$LHAPATH"share/LHAPDF/

# Cleanup
rm $wd/apfel*

# cp to final_pdf generation area for splitting/sorting, and final_pdf generation
rm -rf /home/narendran/Documents/CEDAR_stuff2/photon-pdf-generation/SCRIPTS_FOR_FINAL_PDFS/ALL_RUNS/RUN$RUN_NUM
mkdir -p /home/narendran/Documents/CEDAR_stuff2/photon-pdf-generation/SCRIPTS_FOR_FINAL_PDFS/ALL_RUNS/RUN$RUN_NUM
cp -r $wd/prof-parametersR$RUN_NUM /home/narendran/Documents/CEDAR_stuff2/photon-pdf-generation/SCRIPTS_FOR_FINAL_PDFS/ALL_RUNS/RUN$RUN_NUM/

rm $wd/apfel*
## cd to LHAgraphing to validate pdfs if neccesary

## cd to sherpa to run with new pdfs
