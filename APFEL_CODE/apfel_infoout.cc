#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <vector>
#include "APFEL/APFEL.h"
using namespace std;

inline string getEnv(const string& name, const string& fallback="ERR") {
  const char* env = std::getenv(name.c_str());
  return env ? string(env) : fallback;
}

template <typename T>
inline T getEnv(const string& name) {
  const string senv = getEnv(name);
  T var;
  istringstream iss;
  iss.str(senv);
  iss >> var;
  if (iss.fail()) throw runtime_error("Invalid env variable");
  return var;
}

template <typename T>
inline T getEnv(const string& name, const T& fallback) {
  const string senv = getEnv(name);
  T var;
  try {
    istringstream iss;
    iss.str(senv);
    iss >> var;
    if (iss.fail()) return fallback;
  } catch (...) {
    return fallback;
  }
  return var;
}

// A linspace analogue
std::vector<double> linspace(double start, double end, int num) {
  std::vector<double> result;
  if (num <= 0) return result;
  if (num == 1) {
    result.push_back(start);
    return result;
  }

  double step = (end - start) / (num - 1);
  for (int i = 0; i < num; ++i) {
    result.push_back(start + i * step);
  }
  return result;
}

// A logspace analogue
std::vector<double> logspace(double start, double end, int num) {
  std::vector<double> result;
  if (num <= 0) return result;
  if (num == 1) {
    result.push_back(pow(10, start));
    return result;
  }

  double log_start = log10(start);
  double log_end = log10(end);
  double step = (log_end - log_start) / (num - 1);
  for (int i = 0; i < num; ++i) {
    result.push_back(pow(10, log_start + i * step));
  }
  return result;
}


extern "C" void externalsetapfel_(const double& x, const double& /* Q */, double* xf) {
  const double Apl = getEnv<double>("PDF_APL", 4.0);
  const double Bpl = getEnv<double>("PDF_BPL", 1.2);
  const double Ahad = getEnv<double>("PDF_AHAD", 0.22);
  const double Bhad = getEnv<double>("PDF_BHAD", -1.17);
  const double Chad = getEnv<double>("PDF_CHAD", 1.0);
  const double Ahadg = getEnv<double>("PDF_AHADG", 0.07);
  const double Bhadg = getEnv<double>("PDF_BHADG", -1.64);
  const double Chadg = getEnv<double>("PDF_CHADG", 3.0);

  const double Ahad_log = 0.1 * (pow(10, Ahad) - 1);
  const double Ahadg_log = 0.1 * (pow(10, Ahadg) - 1);
  const double xbar = 1 - x;
  const double f_pl_e2 = Apl * (x*x + xbar*xbar) / (1 - Bpl*log(xbar));
  const double f_had_ud = Ahad_log * pow(x, Bhad) * pow(xbar, Chad);
  const double f_had_s = 0.3 * f_had_ud;
  const double f_had_g = Ahadg_log * pow(x, Bhadg) * pow(xbar, Chadg);

  xf[0] = 0.0; // tbar
  xf[1] = 0.0; // bbar
  xf[2] = 0.0; // cbar
  xf[3] = x * (1/9. * f_pl_e2 + f_had_s); // sbar
  xf[4] = x * (4/9. * f_pl_e2 + f_had_ud); // ubar
  xf[5] = x * (1/9. * f_pl_e2 + f_had_ud); // dbar
  xf[6] = x * f_had_g; // gluon
  xf[7] = xf[5]; // d
  xf[8] = xf[4]; // u
  xf[9] = xf[3]; // s
  xf[10] = 0.0; // c
  xf[11] = 0.0; // b
  xf[12] = 0.0; // t
  xf[13] = 0.0; // gamma
}


int main() {

  // X points
  // Log spaced array for xs
  double x_log_start = 1e-5;
  double x_log_end = 1e-1;
  int x_log_num = 50;
  std::vector<double> x_log_array = logspace(x_log_start, x_log_end, x_log_num);

  // Lin spaced array for high x
  double x_lin_start = 1e-1;
  double x_lin_end = 1.0;
  int x_lin_num = 100;
  std::vector<double> x_lin_array = linspace(x_lin_start, x_lin_end, x_lin_num);

  // full xs array
  std::vector<double> xs = x_log_array;
  xs.insert(xs.end(), next(x_lin_array.begin()), x_lin_array.end());

  

  // log spaced array for Q2s
  double Q2_log_start = 2.0;
  double Q2_log_end = 1e4;
  int Q2_log_num = 50;
  std::vector<double> Q2_log_array = logspace(Q2_log_start, Q2_log_end, Q2_log_num);

  // Full Qs array (only log sampled for now)
  std::vector<double> Q2s = Q2_log_array;

  const std::vector<int> flavours = {-6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6, 21};

  // Activate some options
  APFEL::SetPerturbativeOrder(1);
  APFEL::SetPDFSet("external");

  // Initialize integrals on the grids
  APFEL::InitializeAPFEL();

  // Get some info and set some variables
  const int num_members = getEnv<int>("num_members");
  const string pdf_name = getEnv<string>("pdf_name");
  
  // Write info file
  ofstream infofile (pdf_name + ".info");
  
  infofile << "SetDesc: """ << endl;
  infofile << "Authors: AB VAN" << endl;
  infofile << "Reference:" << endl;
  infofile << "Format: lhagrid1" << endl;
  infofile << "DataVersion: -1" << endl;
  infofile << "NumMembers: " << num_members << endl;
  infofile << "Particle: 22" << endl;
  
  infofile << "Flavors: [";
  for (auto flavour = flavours.begin(); flavour != flavours.end() -1; ++flavour) {
    infofile << setprecision(0) << *flavour << ", ";
  }
  infofile << setprecision(0) << flavours.back() << "]" << endl;

  // This next line OrderQCD needs lots of verification
  infofile << "OrderQCD: 1" << endl;
  infofile << "FlavorScheme: variable" << endl;
  infofile << "NumFlavors: 6" << endl;
  
  infofile << "XMin: " << xs[0] << endl;
  infofile << "XMax: " << xs.back() << endl;
  
  infofile << "QMin: " << Q2s[0] << endl;
  infofile << "QMax: " << Q2s.back() << endl;

  // Mass info and AlphaS info goes here
  // Also need to find a way to add masses directly from apfel rather than typing in. Check documentation!!
  infofile << "MCharm: 1.4142" << endl;
  infofile << "MBottom: 4.5000" << endl;
  infofile << "MTop: 175.000" << endl;

  // These numbers are currently just being thrown in to get it working. Correct values need to be added in before running on batch system.
  infofile << "AlphaS_MZ: 0.1188" << endl;
  infofile << "AlphaS_OrderQCD: 1" << endl;
  infofile << "AlphaS_Type: ipol" << endl;

  infofile << "AlphaS_Qs: [";
  for (auto Q = Q2s.begin(); Q != Q2s.end() - 1; ++Q) {
    infofile << scientific << setprecision(6) << *Q << ", ";
  }
  infofile << scientific << setprecision(6) << Q2s.back() << "]" << endl;

  infofile << "AlphaS_Vals: [";
  for (auto Q = Q2s.begin(); Q != Q2s.end() - 1; ++Q) {
    infofile << scientific << setprecision(6) << APFEL::AlphaQCD(sqrt(*Q)) << ", ";
  }
  infofile << scientific << setprecision(6) << APFEL::AlphaQCD(sqrt(Q2s.back())) << "]" << endl;

  // alphas_lambda terms here again being added to get it working. Correct values before proper run!!
  infofile << "AlphaS_Lambda4: 0.2" << endl;
  infofile << "AlphaS_Lambda5: 0.3226" << endl;

  infofile.close();
  return 0;
}
