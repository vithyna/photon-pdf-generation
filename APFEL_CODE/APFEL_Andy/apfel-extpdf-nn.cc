#include "APFEL/APFEL.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <exception>
using namespace std;

#include "LHAPDF/LHAPDF.h"
LHAPDF::PDF* lhp = nullptr;

/// @todo Questions about reference and array-offset mappings...
extern "C" void externalsetapfel_(const double& x, const double& Q, double* xf) {
  // static LHAPDF::PDF* lhp = LHAPDF::mkPDF("NNPDF31_nlo_as_0118", 0);
  for (int ifl = -6; ifl < 7; ++ifl) {
    xf[ifl+6] = lhp->xfxQ(ifl, x, Q); // antiquarks, gluon, quarks
  }
  xf[13] = 0.0; // gamma
}


int main() {

  if (!lhp) lhp = LHAPDF::mkPDF("NNPDF31_nlo_as_0118", 0);

  // Grid points
  const double Qs[] = {1.0, 10.0, 50.0, 100.0};
  const double xs[] = {1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 3e-1, 5e-1, 7e-1, 9e-1};
  const double Qs_alphaS[] = {      1.,  3.43,   5.86,   8.29,  10.71,  13.14,  15.57,  18.  ,
                                20.43,  22.86,  25.29,  27.71,  30.14,  32.57,  35.  ,  37.43,
                                39.86,  42.29,  44.71,  47.14,  49.57,  52.  ,  54.43,  56.86,
                                59.29,  61.71,  64.14,  66.57,  69.  ,  71.43,  73.86,  76.29,
                                78.71,  81.14,  83.57,  86.  ,  88.43,  90.86,  93.29,  95.71,
                                98.14, 100.57, 103.  , 105.43, 107.86, 110.29, 112.71, 115.14,
                                117.57, 120.};

  // Activate some options
  APFEL::SetPerturbativeOrder(1);
  APFEL::SetAlphaQCDRef(lhp->alphasQ(91.1876), 91.1876);
  APFEL::SetPoleMasses(lhp->quarkThreshold(4), lhp->quarkThreshold(5), lhp->quarkThreshold(6));
  APFEL::SetMaxFlavourAlpha(5);
  APFEL::SetMaxFlavourPDFs(5);
  APFEL::SetAlphaEvolution("expanded");
  APFEL::SetPDFEvolution("truncated");
  //APFEL::SetPDFSet("external");
  APFEL::SetPDFSet("NNPDF31_nlo_as_0118");
  //APFEL::SetNumberOfGrids(3);
  //APFEL::SetGridParameters(1,100,3,1e-5);
  //APFEL::SetGridParameters(2,70,5,1e-1);
  //APFEL::SetGridParameters(3,40,5,8e-1);
  /// @todo Set alpha_s

  // Initialize integrals on the grids
  APFEL::InitializeAPFEL();

  // Load evolution
  const double Q0 = 1.65 - 1e-10;
  for (const double Q : Qs) {
    APFEL::EvolveAPFEL(Q0, Q);

    // Tabulate PDFs for the LHA x values
    cout << "Q = " << Q << " GeV" << endl;
    cout << "alpha_QCD(mu2F) = " << APFEL::AlphaQCD(Q) << endl;
    // cout << "alpha_QED(mu2F) = " << APFEL::AlphaQED(Q) << endl;

    // Print alpha_s (should not depend on PDF evolution, but print out at each point, to check)
    cout << "alpha_QCD(Q):";
    cout << " Q " << setprecision(4);
    for (const double& Q : Qs_alphaS) cout << setw(12) << Q << ", ";
    cout << endl;
    cout << "aS " << setprecision(4);
    for (const double& Q : Qs_alphaS) cout << setw(12) << APFEL::AlphaQCD(Q) << ", ";
    cout << endl;
    cout << "aS (LHAPDF)" << setprecision(4);
    for (const double& Q : Qs_alphaS) cout << setw(12) << lhp->alphasQ(Q) << ", ";
    cout << endl << endl;

    cout << scientific << "          x " << setprecision(4);
    for (double x : xs) cout << setw(12) << x << ", ";
    cout << endl;
    //
    for (int ifl = -6; ifl < 7; ++ifl) {
      cout << setw(12) << ifl;
      for (double x : xs) {
        cout << scientific << setw(12) << APFEL::xPDFj(ifl,x) << ", ";
      }
      cout << endl;
      //
      cout << "            ";
      for (double x : xs) {
        cout << scientific << setw(12) << lhp->xfxQ(ifl,x,Q) << ", ";
      }
      cout << endl;
    }
    cout << endl;
  }

  return 0;
}
