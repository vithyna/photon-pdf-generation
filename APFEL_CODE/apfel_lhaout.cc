#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <vector>
#include "APFEL/APFEL.h"
using namespace std;


inline string getEnv(const string& name, const string& fallback="ERR") {
  const char* env = std::getenv(name.c_str());
  return env ? string(env) : fallback;
}

template <typename T>
inline T getEnv(const string& name) {
  const string senv = getEnv(name);
  T var;
  istringstream iss;
  iss.str(senv);
  iss >> var;
  if (iss.fail()) throw runtime_error("Invalid env variable:" + name);
  return var;
}

template <typename T>
inline T getEnv(const string& name, const T& fallback) {
  const string senv = getEnv(name);
  T var;
  try {
    istringstream iss;
    iss.str(senv);
    iss >> var;
    if (iss.fail()) return fallback;
  } catch (...) {
    return fallback;
  }
  return var;
}

// A linspace analogue
std::vector<double> linspace(double start, double end, int num) {
  std::vector<double> result;
  if (num <= 0) return result;
  if (num == 1) {
    result.push_back(start);
    return result;
  }

  double step = (end - start) / (num - 1);
  for (int i = 0; i < num; ++i) {
    result.push_back(start + i * step);
  }
  return result;
}

// A logspace analogue
std::vector<double> logspace(double start, double end, int num) {
  std::vector<double> result;
  if (num <= 0) return result;
  if (num == 1) {
    result.push_back(pow(10, start));
    return result;
  }

  double log_start = log10(start);
  double log_end = log10(end);
  double step = (log_end - log_start) / (num - 1);
  for (int i = 0; i < num; ++i) {
    result.push_back(pow(10, log_start + i * step));
  }
  return result;
}


extern "C" void externalsetapfel_(const double& x, const double& /* Q */, double* xf) {
  const double Apl = getEnv<double>("PDF_APL");
  const double Bpl = getEnv<double>("PDF_BPL");
  const double Ahad = getEnv<double>("PDF_AHAD");
  const double Bhad = getEnv<double>("PDF_BHAD");
  const double Chad = getEnv<double>("PDF_CHAD", 1.0);
  const double Ahadg = getEnv<double>("PDF_AHADG");
  const double Bhadg = getEnv<double>("PDF_BHADG");
  const double Chadg = getEnv<double>("PDF_CHADG", 3.0);

  const double Ahad_log = 0.1 * (pow(10, Ahad) - 1);
  const double Ahadg_log = 0.1 * (pow(10, Ahadg) - 1);
  
  // Try Apl_log as well - cross-check with Andy
  const double xbar = 1 - x;
  const double f_pl_e2 = Apl * (x*x + xbar*xbar) / (1 - Bpl*log(xbar));
  const double f_had_ud = Ahad_log * pow(x, Bhad) * pow(xbar, Chad);
  const double f_had_s = 0.3 * f_had_ud;
  const double f_had_g = Ahadg_log * pow(x, Bhadg) * pow(xbar, Chadg);

  xf[0] = 0.0; // tbar
  xf[1] = 0.0; // bbar
  xf[2] = 0.0; // cbar
  xf[3] = x * (1/9. * f_pl_e2 + f_had_s); // sbar
  xf[4] = x * (4/9. * f_pl_e2 + f_had_ud); // ubar
  xf[5] = x * (1/9. * f_pl_e2 + f_had_ud); // dbar
  xf[6] = x * f_had_g; // gluon
  xf[7] = xf[5]; // d
  xf[8] = xf[4]; // u
  xf[9] = xf[3]; // s
  xf[10] = 0.0; // c
  xf[11] = 0.0; // b
  xf[12] = 0.0; // t
  xf[13] = 0.0; // gamma
}


int main() {
  
  const string mem_number = getEnv<string>("mem_number");
  const string pdf_name = getEnv<string>("pdf_name");

  // Create .dat file to write to
  ofstream outfile (pdf_name + "_" + mem_number + ".dat");
  outfile << "PdfType: central" << endl;
  outfile << "Format: lhagrid1" << endl;
  outfile << "---" << endl;

  // X points
  // Log spaced array for xs
  double x_log_start = 1e-5;
  double x_log_end = 1e-1;
  int x_log_num = 50;
  std::vector<double> x_log_array = logspace(x_log_start, x_log_end, x_log_num);

  // Lin spaced array for high x
  double x_lin_start = 1e-1;
  double x_lin_end = 1.0;
  int x_lin_num = 100;
  std::vector<double> x_lin_array = linspace(x_lin_start, x_lin_end, x_lin_num);

  // full xs array
  std::vector<double> xs = x_log_array;
  xs.insert(xs.end(), next(x_lin_array.begin()), x_lin_array.end());



  // log spaced array for Q2s
  // LHAPDF takes grids in xs, Q2s
  double Q2_log_start = 2.0;
  double Q2_log_end = 1e4;
  int Q2_log_num = 50;
  std::vector<double> Q2_log_array = logspace(Q2_log_start, Q2_log_end, Q2_log_num);

  // Full Q2s array (only log sampled for now)
  std::vector<double> Q2s = Q2_log_array;

  const std::vector<int> flavours = {-6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6, 21};

  // Activate some options
  APFEL::SetPerturbativeOrder(1);
  APFEL::SetPDFSet("external");

  //APFEL::SetNumberOfGrids(3);
  //APFEL::SetGridParameters(1, 80, 3, 1e-5);
  //APFEL::SetGridParameters(2, 100, 5, 1e-1);
  //APFEL::SetGridParameters(3, 40, 5, 8e-1);

  // Initialize integrals on the grids
  APFEL::InitializeAPFEL();

  // write out x values
  for (const double x : xs) {
    outfile << scientific << setprecision(6) << x << " ";
  }
  outfile << endl;
  for (const double Q2 : Q2s) {
    outfile << scientific << setprecision(6) << Q2 << " ";
  }
  outfile << endl;
  for (const int flavour : flavours) {
    outfile << setprecision(0) << flavour << " ";
  }
  outfile << endl;


  // Load evolution
  const double Q02 = Q2s[0] - 1e-10;
  //const double Q0 = 2.0 - 1e-10;
  const double Q0 = sqrt(Q02);
  for (const double x : xs) {
    for (const double Q2 : Q2s) {
      double Q = sqrt(Q2);
      APFEL::EvolveAPFEL(Q0, Q);
      //cout << "alpha_QCD(mu2F) = " << APFEL::AlphaQCD(Q) << endl;
      //cout << "alpha_QED(mu2F) = " << APFEL::AlphaQED(Q) << endl;

      outfile << scientific << setprecision(6) 
              << APFEL::xPDF(-6,x) << " "
              << APFEL::xPDF(-5,x) << " "
              << APFEL::xPDF(-4,x) << " "
              << APFEL::xPDF(-3,x) << " "
              << APFEL::xPDF(-2,x) << " "
              << APFEL::xPDF(-1,x) << " "
              << APFEL::xPDF(1,x) << " "
              << APFEL::xPDF(2,x) << " "
              << APFEL::xPDF(3,x) << " "
              << APFEL::xPDF(4,x) << " "
              << APFEL::xPDF(5,x) << " "
              << APFEL::xPDF(6,x) << " "
              << APFEL::xPDF(0,x) << " " << endl;
    }
  }
  outfile << "---" << endl;
  outfile.close();

  return 0;
}
